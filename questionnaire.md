Questions
---------

* Age
* Gender
* Education

1. Would you likely use a digital currency?
  1. If yes, for which reasons?
    * Shopping
    * Gambling
    * Private money transfers
    * Deposit
2. Have you heard about bitcoin or any alternative?
  1. If yes, where did you hear about it?
    * Tv
    * Social Media
    * Internet news website
3. Do you play online games?
4. How difficult does it seem to use? 1-4
5. How safe does it look like? 1-4
7. Would you support a switch of the official currency of you country to
   a digital one?
