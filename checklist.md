Readiness Checklist
-------------------

1. The presentation has to be publicly available and served so that it can
be accessed from every computer.
2. Laptop
3. Laptop charger.
4. Mobile Phone
5. Mobile Phone usb cable.

Before Presenting
-----------------

1. Check for internet connection.
2. Have enough cell phone credit to enable 3G if needed.
3. Start a local server.
4. Be awesome!
